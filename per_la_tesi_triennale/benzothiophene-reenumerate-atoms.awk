#!/usr/bin/awk -f

BEGIN {
    FS = OFS = "\t"

    ex_at=4        # Set the field that holds the excited atom (the one from which the transition starts).
    at_pos=10      # Set the field that holds the position number of the atom to which the AO contributing to a MO belongs.
    at_el=11       # Set the field that holds the element of the atom to which the contributing AO belongs.
    ex_lbl=".1"    # Set the label that refers to the excited atom

    # # Replacement table for the excited atoms
    # # it defines how they will be renamed, later
    # # NB: not needed if the atoms numeration has already been fixed in the spreadsheet (and therefore in the tsv file)
    # replacement["C1"] = "C4"
    # replacement["C2"] = "C3"
    # replacement["C3"] = "C5"
    # replacement["C4"] = "C2"
    # replacement["C5"] = "C8"
    # # C6 is the same
    # # C7 is the same
    # replacement["C8"] = "C1"
}

# # Rename the excited atoms
# # see https://stackoverflow.com/questions/48510221/run-several-string-substitutions-safely
# # NB: not needed if the atoms are renamed in the spreadsheet
# $ex_at in replacement {$ex_at = replacement[$ex_at]}

# Remove the labels that indicate the excited atom in ADF
{sub(ex_lbl, "", $at_el)}

# Store the excited atom of the transition in the variable exc_at
$ex_at != "" {exc_at = $ex_at}

# Sulphur
$at_pos == 1 && $at_el == "S" {$at_pos = ""}

# Carbon atoms
{
    if (exc_at == "C1") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 5 && $at_el == "C") {$at_pos = 2}
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 5}
        # C7 is the same
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 1}
    }
    else if (exc_at == "C2") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 8}
        # C5 is the same
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 7 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 2}
    }
    else if (exc_at == "C3") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        else if ($at_pos == 3 && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 2}
        # C5 is the same
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 7 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 3}
    }
    else if (exc_at == "C4") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 5 && $at_el == "C") {$at_pos = 2}
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 5}
        # C7 is the same
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 1}
    }
    else if (exc_at == "C5") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 5 && $at_el == "C") {$at_pos = 2}
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 7 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 5}
    }
    else if (exc_at == "C6") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 5 && $at_el == "C") {$at_pos = 2}
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 5}
        # C7 is the same
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 6}
    }
    else if (exc_at == "C7") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 5 && $at_el == "C") {$at_pos = 2}
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 5}
        else if ($at_pos == 7 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 7}
    }
    else if (exc_at == "C8") {
        if      ($at_pos == 2 && $at_el == "C") {$at_pos = 4}
        # C3 is the same
        else if ($at_pos == 4 && $at_el == "C") {$at_pos = 2}
        # C5 is the same
        else if ($at_pos == 6 && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 7 && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8 && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 9 && $at_el == "C") {$at_pos = 8}
    }
}

# Hydrogen atoms
$at_pos == 10 && $at_el == "H" {$at_pos = 3}
$at_pos == 11 && $at_el == "H" {$at_pos = 2}
$at_pos == 12 && $at_el == "H" {$at_pos = 6}
$at_pos == 13 && $at_el == "H" {$at_pos = 4}
$at_pos == 14 && $at_el == "H" {$at_pos = 5}
$at_pos == 15 && $at_el == "H" {$at_pos = 1}

{print}
