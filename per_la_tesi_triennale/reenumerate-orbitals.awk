#!/usr/bin/awk -f

# Only for calculations made with the frozen core on the S atom(s) and the non-excited C atoms.

BEGIN {
    FS = OFS = "\t"
    ex_at=4      # Set the field that holds the excited atom (the one from which the transition starts).
    n=8          # Set the field that holds the principal quantum number of the AO contributing to a MO.
    AO_l=9       # Set the field that holds the type of orbital (s, p, d, f...) of the contributing AO.
    at_pos=10    # Set the field that holds the position number of the atom to which the contributing AO belongs.
    at_el=11     # Set the field that holds the element of the atom to which the contributing AO belongs.
}

$ex_at != "" {exc_at = $ex_at}                 # Store the excited atom of the transition in the variable exc_at
{contr_at = $at_el$at_pos}                     # Store the atom that contributes the AO, in the form C#, in the variable contr_at

# Sulphur
$AO_l == "S" && $at_el == "S" {$n = $n + 2}    # Increase "n" by 2 for every S s orbital (because the orbitals 1s and 2s are part of the frozen core)
$AO_l ~ /P:/ && $at_el == "S" {$n = $n + 2}    # Increase "n" by 2 for every S p orbital (1, plus 1 because of the 2p that is part of the frozen core)
$AO_l ~ /D:/ && $at_el == "S" {$n = $n + 2}    # Increase "n" by 2 for every S d orbital
$AO_l ~ /F:/ && $at_el == "S" {$n = $n + 3}    # Increase "n" by 3 for every S f orbital

# Carbon
# If an s orbital doesn’t belong to the excited carbon, increase "n" by 1 (because the carbon has the frozen core (1s));
# otherwise do nothing, the principal quantum number is ok as it is.
$AO_l == "S" && $at_el == "C" && exc_at != contr_at {$n = $n + 1}
$AO_l ~ /P:/ && $at_el == "C" {$n = $n + 1}    # Increase "n" by 1 for every C p orbital
$AO_l ~ /D:/ && $at_el == "C" {$n = $n + 2}    # Increase "n" by 2 for every C d orbital
$AO_l ~ /F:/ && $at_el == "C" {$n = $n + 3}    # Increase "n" by 3 for every C f orbital

# Hydrogen
$AO_l ~ /P:/ && $at_el == "H" {$n = $n + 1}    # Increase "n" by 1 for every H p orbital
$AO_l ~ /D:/ && $at_el == "H" {$n = $n + 2}    # Increase "n" by 2 for every H d orbital
$AO_l ~ /F:/ && $at_el == "H" {$n = $n + 3}    # Increase "n" by 3 for every H f orbital

{print}
