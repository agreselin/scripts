#!/usr/bin/awk -f

BEGIN {
    FS = OFS = "\t"
    ex_at=4        # Set the field that holds the excited atom (the one from which the transition starts).
    at_pos=10      # Set the field that holds the position number of the atom to which the AO contributing to a MO belongs.
    at_el=11       # Set the field that holds the element of the atom to which the contributing AO belongs.
    ex_lbl=".1"    # Set the label that refers to the excited atom
}

# Remove the labels that indicate the excited atom in ADF
{sub(ex_lbl, "", $at_el)}

# Store the excited atom of the transition in the variable exc_at
$ex_at != "" {exc_at = $ex_at}

# Sulphur
$at_pos == 1 && $at_el == "S" {$at_pos = ""}

# Carbon atoms
{
    if (exc_at == "C1") {
        # C2 is the same
        if      ($at_pos == 3  && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4  && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 5  && $at_el == "C") {$at_pos = 3}
        else if ($at_pos == 6  && $at_el == "C") {$at_pos = 9}
        else if ($at_pos == 7  && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8  && $at_el == "C") {$at_pos = 12}
        else if ($at_pos == 9  && $at_el == "C") {$at_pos = 4}
        # C10 is the same
        else if ($at_pos == 11 && $at_el == "C") {$at_pos = 5}
        else if ($at_pos == 12 && $at_el == "C") {$at_pos = 11}
        else if ($at_pos == 13 && $at_el == "C") {$at_pos = 1}
    }
    if (exc_at == "C2") {
        # C2 is the same
        if      ($at_pos == 3  && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4  && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 5  && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 6  && $at_el == "C") {$at_pos = 3}
        else if ($at_pos == 7  && $at_el == "C") {$at_pos = 9}
        else if ($at_pos == 8  && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 9  && $at_el == "C") {$at_pos = 12}
        else if ($at_pos == 10 && $at_el == "C") {$at_pos = 4}
        else if ($at_pos == 11 && $at_el == "C") {$at_pos = 10}
        else if ($at_pos == 12 && $at_el == "C") {$at_pos = 5}
        else if ($at_pos == 13 && $at_el == "C") {$at_pos = 11}
    }
    if (exc_at == "C3") {
        # C2 is the same
        if      ($at_pos == 3  && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4  && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 5  && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 6  && $at_el == "C") {$at_pos = 9}
        else if ($at_pos == 7  && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 8  && $at_el == "C") {$at_pos = 12}
        else if ($at_pos == 9  && $at_el == "C") {$at_pos = 4}
        # C10 is the same
        else if ($at_pos == 11 && $at_el == "C") {$at_pos = 5}
        else if ($at_pos == 12 && $at_el == "C") {$at_pos = 11}
        else if ($at_pos == 13 && $at_el == "C") {$at_pos = 3}
    }
    if (exc_at == "C4") {
        # C2 is the same
        if      ($at_pos == 3  && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4  && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 5  && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 6  && $at_el == "C") {$at_pos = 3}
        else if ($at_pos == 7  && $at_el == "C") {$at_pos = 9}
        else if ($at_pos == 8  && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 9  && $at_el == "C") {$at_pos = 12}
        # C10 is the same
        else if ($at_pos == 11 && $at_el == "C") {$at_pos = 5}
        else if ($at_pos == 12 && $at_el == "C") {$at_pos = 11}
        else if ($at_pos == 13 && $at_el == "C") {$at_pos = 4}
    }
    if (exc_at == "C5") {
        # C2 is the same
        if      ($at_pos == 3  && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4  && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 5  && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 6  && $at_el == "C") {$at_pos = 3}
        else if ($at_pos == 7  && $at_el == "C") {$at_pos = 9}
        else if ($at_pos == 8  && $at_el == "C") {$at_pos = 6}
        else if ($at_pos == 9  && $at_el == "C") {$at_pos = 12}
        else if ($at_pos == 10 && $at_el == "C") {$at_pos = 4}
        else if ($at_pos == 11 && $at_el == "C") {$at_pos = 10}
        else if ($at_pos == 12 && $at_el == "C") {$at_pos = 11}
        else if ($at_pos == 13 && $at_el == "C") {$at_pos = 5}
    }
    if (exc_at == "C6") {
        # C2 is the same
        if      ($at_pos == 3  && $at_el == "C") {$at_pos = 8}
        else if ($at_pos == 4  && $at_el == "C") {$at_pos = 1}
        else if ($at_pos == 5  && $at_el == "C") {$at_pos = 7}
        else if ($at_pos == 6  && $at_el == "C") {$at_pos = 3}
        else if ($at_pos == 7  && $at_el == "C") {$at_pos = 9}
        else if ($at_pos == 8  && $at_el == "C") {$at_pos = 12}
        else if ($at_pos == 9  && $at_el == "C") {$at_pos = 4}
        # C10 is the same
        else if ($at_pos == 11 && $at_el == "C") {$at_pos = 5}
        else if ($at_pos == 12 && $at_el == "C") {$at_pos = 11}
        else if ($at_pos == 13 && $at_el == "C") {$at_pos = 6}
    }
}

# Hydrogen atoms
$at_pos == 14 && $at_el == "H" {$at_pos = 1}
$at_pos == 15 && $at_el == "H" {$at_pos = 5}
$at_pos == 16 && $at_el == "H" {$at_pos = 4}
$at_pos == 17 && $at_el == "H" {$at_pos = 8}
$at_pos == 18 && $at_el == "H" {$at_pos = 2}
$at_pos == 19 && $at_el == "H" {$at_pos = 6}
$at_pos == 20 && $at_el == "H" {$at_pos = 3}
$at_pos == 21 && $at_el == "H" {$at_pos = 7}

{print}
