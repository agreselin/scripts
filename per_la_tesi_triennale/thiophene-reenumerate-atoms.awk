#!/usr/bin/awk -f

BEGIN {
    FS = OFS = "\t"
    ex_at=4     # Set the field that holds the excited atom (the one from which the transition starts).
    at_pos=10   # Set the field that holds the position number of the atom to which the AO contributing to a MO belongs.
    at_el=11    # Set the field that holds the element of the atom to which the contributing AO belongs.
}

$ex_at != "" {exc_at = $ex_at}                  # Store the excited atom of the transition in the variable exc_at

# Suphur
$at_pos == 1 && $at_el == "S" {$at_pos = ""}    # Replace 1 S with S

# Carbon atoms
{
    if      ($at_pos == 2 && $at_el == "C")   {$at_pos = 1; $at_el = "C"}                    # Replace 2 C with 1 C
    else if ($at_pos == 2 && $at_el == "C.1") {$at_pos = 1; $at_el = "C"}                    #         2 C.1    1 C
    else if ($at_pos == 5 && $at_el == "C")   {$at_pos = 4; $at_el = "C"}                    #         5 C      4 C
    else if ($at_pos == 5 && $at_el == "C.1") {$at_pos = 2; $at_el = "C"}                    #         5 C.1    2 C
    else if ($at_pos == 4 && $at_el == "C" && exc_at == "C1") {$at_pos = 2; $at_el = "C"}    # Replace 4 C with 2 C if the excited atom is C1, otherwise leave it as it is.
}

# Hydrogen atoms
$at_pos == 6 && $at_el == "H" {$at_pos = 1}
$at_pos == 7 && $at_el == "H" {$at_pos = 3}
$at_pos == 8 && $at_el == "H" {$at_pos = 2}
$at_pos == 9 && $at_el == "H" {$at_pos = 4}

{print}
