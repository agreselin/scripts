#!/usr/bin/env bash

# 'man 7z' discourages from using 7z for backup purposes on Linux/Unix
# because 7-zip does not store the owner/group of files.
# It suggests to use the following commands to make backups on Linux/Unix:
#  - to backup a directory:  tar cf - directory | 7z a -si directory.tar.7z
#  - to restore your backup: 7z x -so directory.tar.7z | tar xf -
# This script implements the first of these two commands.

# Limitations:
# It only works on one file or directory at a time.

if [[ $# = 0 ]]; then
  echo >&2 "Usage:"
  echo >&2 "  tar7z <file or directory> <archive name without extension>"
  echo >&2 "The second argument is optional. If it is absent, the first one is"
  echo >&2 "used in its place, with any trailing \"/\" automatically removed."
  exit 1
fi

if [[ -e "$1" ]]; then
  # The sed command removes the trailing "/" if there is one.
  tar cf - "$1" | 7z a -si "$(sed 's,/$,,g' <<< "${2:-$1}")".tar.7z
else
  echo >&2 "Error: $1 not found."
  exit 2
fi
