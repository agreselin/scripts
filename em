#!/bin/env bash

# Open files in an existing Emacs frame or create a new one.
#
# By default the frame is 80 columns wide and 40 rows high. With option '-m'
# it is maximized instead.
#
# https://superuser.com/questions/358037/emacsclient-create-a-frame-if-a-frame-does-not-exist/862809#862809
# https://www.gnu.org/software/emacs/manual/html_node/emacs/emacsclient-Options.html

frame_parameters="((height . 40) (width . 80))"

while getopts ":m" opt; do
    case $opt in
        m)
            frame_parameters="((fullscreen . maximized))"
            ;;
        \?)
            echo "${0##*/} error: Invalid option, '-$OPTARG'. Valid options:" \
                 "'-m'." >&2
            exit 2
            ;;
    esac
done
shift "$((OPTIND - 1))"

# If there is an Emacs frame lying around the following 'emacsclient' command
# returns t and Grep exits with status 0; if there isn't any Emacs frame in any
# workspace Grep exits with status 1.
emacsclient --no-wait --eval "(> (length (frame-list)) 1)" \
    | grep --quiet t

if [[ "$?" == 0 ]]; then
    # Raise the existing Emacs frame.
    emacsclient \
        --no-wait \
        --alternate-editor= \
        --frame-parameters="$frame_parameters" "$@"
else
    # Open a new frame.
    emacsclient \
        --create-frame \
        --no-wait \
        --alternate-editor= \
        --frame-parameters="$frame_parameters" "$@"
fi
