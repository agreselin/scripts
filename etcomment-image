#!/usr/bin/env bash

# Usage
#
#   etcomment-image <file or directory> <comment>
#
# Move any Windows comment (stored in the tag 'xpcomment') to the tag
# 'Description' of the specified file, or of all files under the specified
# directory, then add '<comment>' to that tag. Also write the tag
# 'Caption-Abstract' if it is already non-empty or if the file is a PNG image.
#
# exiftool puts a length limit of 2000 characters on the content of the
# 'Caption-Abstract' tag. I write it anyway because it's the only one that
# Geeqie and gThumb show for PNG images. I create it only for PNG images because
# with other file types gThumb displays it preferentially but falls back to
# 'Description' when 'Caption-Abstract' is empty (tested with JPG and TIFF).
# Therefore creating it might make gThumb override an untrimmed 'Description'
# with a trimmed 'Caption-Abstract'.

# Tags written and read by Geeqie and gThumb
#
#         Image Description   Description   User Comment*  Caption-Abstract
# Geeqie            W: J, P       W: J, P        W:                 W:
#  1.5.1            R: J          R:+J           R:                 R: J,+P
#
# gThumb            W:            W: J, P        W: J, P            W: J, P
#  3.8.3            R: J          R: J           R: J               R:+J,+P
#
# Key: W = Writes
#      R = Reads
#      + = Reads preferentially
#      J = JPEG
#      P = PNG
#
# "R" means that the viewer can display that tag (tested with each tag being the
# only non-empty one). "+" means that the viewer displays that tag
# preferentially when each one has a different, non-null value.
#
# * Among the two viewers, gThumb is the only one that reads the 'UserComment'
#   tag, but it only does so for JPG files and it prepends "charset=Ascii" to
#   the displayed comment (though 'exiftool -UserComment ...' doesn't return the
#   "charset=Ascii" part).

# Notes
#
# - $/ makes a new line in a redirection expression;
# - single quotes must be used in Mac/Linux shells around arguments containing a
#   dollar sign;
# - double quotes inside single quotes are for arguments that contain spaces;
# - it may fail to append a comment to a picture that already has one if you
#   put the new comment between quotes in the command line, like
#     etcomment-image ~/Directory "Comment"
#   Try removing the quotes.

# Reference: http://www.sno.phy.queensu.ca/~phil/exiftool/exiftool_pod.html

if [ $# -eq 0 ]; then
  echo >&2 \
"Usage:

  etcomment-image <file or directory> <comment>

To insert a newline in a comment use

  etcomment-image <file or directory> $'Line1\nLine2'

or

  etcomment-image <file or directory> 'Line1
  Line2'

CAUTION: Escape dumb apostrophes in the comment
when you use ANSI-C quoting ($'...')."
#
# Note: to insert a newline,
#   etcomment-image <directory> Line1$/Line2
# works too, but I think the other syntax is more error-proof.
#
  exit 1
fi

echo "Copying Windows comment..."
etcopy-wincomment "$1"

echo "
Erasing the original Windows comment tag and adding the new comment..."
# The pattern here is "If the tag is not empty, append to it; otherwise
# initialize it."
exiftool -if '$Caption-Abstract'                             \
             '-Caption-Abstract<$Caption-Abstract$/$/'"$2"'' \
         -execute                                            \
         -if '$filetype eq "PNG" and not $Caption-Abstract'  \
             '-Caption-Abstract<$Description$/$/'"$2"''      \
         -execute                                            \
         -if '$Description'                                  \
             '-Description<$Description$/$/'"$2"''           \
         -execute                                            \
         -if 'not $Description'                              \
             -Description="$2"                               \
         -execute                                            \
        `# Empty the xpcomment tag, otherwise further calls` \
        `# to this script will keep appending it to the    ` \
        `# other fields.                                   ` \
         -xpcomment=                                         \
         -common_args -preserve "$1"
