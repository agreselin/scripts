#!/usr/bin/awk -f

# Skip the first two rows
FNR>2 {
  # Set the conversion factor
  # 1 Bohr = conversion_factor Angstroms
  # Sources:
  # http://bast.fr/angstrom-bohr/
  # https://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0
  conv=0.52917721067

  # Rename the other fields
  labels = $1
  x = $2
  y = $3
  z = $4

  # Do the conversions
  x=x/conv
  y=y/conv
  z=z/conv

  # Select the output
  {print labels,x,y,z}
}
